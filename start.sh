#CREATE PROJECT DIR
mkdir -p roles/start/tasks/
mkdir -p roles/stop/tasks/
mkdir -p roles/destroy/tasks/
mkdir -p roles/reboot/tasks/
mkdir -p roles/clone/tasks/
mkdir -p host_vars/
mkdir -p group_vars/
mkdir -p play/
#CREATE PROJECT GHOST
echo "#CREATE FILE ZABBIX_HOSTS                                                                      "  > zabbix_hosts.yaml
echo "#CREATE TASK START                                                                             "  > roles/start/tasks/main.yaml
echo "#CREATE TASK STOP                                                                              "  > roles/stop/tasks/main.yaml
echo "#CREATE TASK REBOOT                                                                            "  > roles/reboot/tasks/main.yaml
echo "#CREATE TASK CLONE                                                                             "  > roles/clone/tasks/main.yaml
echo "#CREATE FILE INVENTORY                                                                         "  > inventory.ini 
echo "proxmox ansible_host=10.5.0.70 ansible_user=root                                                "  >> inventory.ini

for (( host=10; host <= 99; host++ ))
do 
echo "   -                                                                                           " >> zabbix_hosts.yaml
echo "      host: 10.0.0.$host                                                                       " >> zabbix_hosts.yaml
echo "      name: machine_10$host                                                                    " >> zabbix_hosts.yaml
echo "      groups:                                                                                  " >> zabbix_hosts.yaml
echo "        -                                                                                      " >> zabbix_hosts.yaml
echo "          name: data-dev                                                                       " >> zabbix_hosts.yaml
echo "      inventory_mode: DISABLED                                                                 " >> zabbix_hosts.yaml
#ANSIBLE INVENTORY           
echo "machine_10$host ansible_host=10.0.0.$host ansible_user=root                                    " >> inventory.ini
#CREATE ANSIBLE HOST VARS
echo " filename         : file10$host.txt                                                            "  > host_vars/machine_10"$host".yaml
echo " hostname         : machine_10$host                                                            " >> host_vars/machine_10"$host".yaml
echo " ip_address       : 10.0.0.$host                                                               " >> host_vars/machine_10"$host".yaml
#CREATE ANSIBLE ROLES FOR PROXMOX SETUP
##CLONE
echo "- name: Cloning machine_10$host                                                                " >> roles/clone/tasks/main.yaml
echo "  command: pct clone 100 10$host                                                               " >> roles/clone/tasks/main.yaml
echo "- name: Setting-up machine_10$host                                                             " >> roles/clone/tasks/main.yaml
echo "  command: pct set 10$host                                                                     " >> roles/clone/tasks/main.yaml      
echo "    -net0 'name=eth0,bridge=vmbr1,gw=10.0.0.1,ip=10.0.0.$host/24'                              " >> roles/clone/tasks/main.yaml  
echo "#NEXT NODE                                                                                     " >> roles/clone/tasks/main.yaml
##STOP
echo "- name: Stoping machine_10$host                                                                " >> roles/stop/tasks/main.yaml
echo "  command: pct stop 10$host                                                                    " >> roles/stop/tasks/main.yaml
echo "#NEXT NODE                                                                                     " >> roles/stop/tasks/main.yaml
##START
echo "- name: Starting machine_10$host                                                               " >> roles/start/tasks/main.yaml
echo "  command: pct start 10$host                                                                   " >> roles/start/tasks/main.yaml
echo "#NEXT NODE                                                                                     " >> roles/start/tasks/main.yaml
##DESTROY
echo "- name: Destroying machine_10$host                                                             " >> roles/destroy/tasks/main.yaml
echo "  command:  pct destroy 10$host                                                                " >> roles/destroy/tasks/main.yaml
echo "#NEXT NODE                                                                                     " >> roles/destroy/tasks/main.yaml
##REBOT
echo "- name: Rebboting machine_10$host                                                              " >> roles/reboot/tasks/main.yaml
echo "  command: pct reboot 10$host                                                                  " >> roles/reboot/tasks/main.yaml
echo "#NEXT NODE                                                                                     " >> roles/reboot/tasks/main.yaml
done

for (( host=100; host <= 254; host++ ))
do 
echo "   -                                                                                           " >> zabbix_hosts.yaml
echo "      host: 10.0.0.$host                                                                       " >> zabbix_hosts.yaml
echo "      name: machine_10$host                                                                    " >> zabbix_hosts.yaml
echo "      groups:                                                                                  " >> zabbix_hosts.yaml
echo "        -                                                                                      " >> zabbix_hosts.yaml
echo "          name: data-prod                                                                      " >> zabbix_hosts.yaml
echo "      inventory_mode: DISABLED                                                                 " >> zabbix_hosts.yaml
#ANSIBLE INVENTORY           
echo "machine_1$host ansible_host=10.0.0.$host ansible_user=root                                     " >> inventory.ini
echo " filename         : file1$host.txt                                                             "  > host_vars/machine_1"$host".yaml
echo " hostname         : machine_1$host                                                             " >> host_vars/machine_1"$host".yaml
echo " ip_address       : 10.0.0.$host                                                               " >> host_vars/machine_1"$host".yaml
#CREATE ANSIBLE ROLES FOR PROXMOX SETUP
##CLONE
echo "- name: Cloning machine_1$host                                                                 " >> roles/clone/tasks/main.yaml
echo "  command: pct clone 100 1$host                                                                " >> roles/clone/tasks/main.yaml
echo "- name: Setting-up machine_1$host                                                              " >> roles/clone/tasks/main.yaml
echo "  command: pct set 1$host                                                                      " >> roles/clone/tasks/main.yaml      
echo "    -net0 'name=eth0,bridge=vmbr1,gw=10.0.0.1,ip=10.0.0.$host/24'                              " >> roles/clone/tasks/main.yaml  
echo "#NEXT NODE                                                                                     " >> roles/clone/tasks/main.yaml
##STOP
echo "- name: Stoping machine_1$host                                                                 " >> roles/stop/tasks/main.yaml
echo "  command: pct stop 1$host                                                                     " >> roles/stop/tasks/main.yaml
echo "#NEXT NODE                                                                                     " >> roles/stop/tasks/main.yaml
##START
echo "- name: Starting machine_1$host                                                                " >> roles/start/tasks/main.yaml
echo "  command: pct start 1$host                                                                    " >> roles/start/tasks/main.yaml
echo "#NEXT NODE                                                                                     " >> roles/start/tasks/main.yaml
##DESTROY
echo "- name: Destroying machine_1$host                                                              " >> roles/destroy/tasks/main.yaml
echo "  command:  pct destroy 1$host                                                                 " >> roles/destroy/tasks/main.yaml
echo "#NEXT NODE                                                                                     " >> roles/destroy/tasks/main.yaml
##REBOT
echo "- name: Rebboting machine_1$host                                                               " >> roles/reboot/tasks/main.yaml
echo "  command: pct reboot 1$host                                                                   " >> roles/reboot/tasks/main.yaml
echo "#NEXT NODE                                                                                     " >> roles/reboot/tasks/main.yaml
done

echo "#LOCAL PROXMOX SERVER                                                                          " >> inventory.ini
echo "[LOCAL]                                                                                        " >> inventory.ini
echo "proxmox                                                                                        " >> inventory.ini
echo "#DATA-DEV CLUSTER 89 SERVERS                                                                   " >> inventory.ini
echo "[DATA-DEV]                                                                                     " >> inventory.ini
for (( host=10; host <= 99; host++ ))
do 
echo "machine_10$host                                                                                " >> inventory.ini
done
echo "#DATA-PROD CLUSTER 154 SERVERS                                                                 " >> inventory.ini
echo "[DATA-PROD]                                                                                    " >> inventory.ini
for (( host=100; host <= 254; host++ ))
do 
echo "machine_1$host                                                                                 " >> inventory.ini
done
